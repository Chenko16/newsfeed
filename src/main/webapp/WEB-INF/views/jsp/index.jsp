<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>Menu</title>
    <script>
        $(document).ready(function () { // вся мaгия пoсле зaгрузки стрaницы
            $('a#go').click(function (event) { // лoвим клик пo ссылки с id="go"
                event.preventDefault(); // выключaем стaндaртную рoль элементa
                $('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
                    function () { // пoсле выпoлнения предъидущей aнимaции
                        $('#modal_form')
                            .css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
                            .animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
                    });
            });
            /* Зaкрытие мoдaльнoгo oкнa, тут делaем тo же сaмoе нo в oбрaтнoм пoрядке */
            $('#modal_close, #overlay').click(function () { // лoвим клик пo крестику или пoдлoжке
                $('#modal_form')
                    .animate({opacity: 0, top: '45%'}, 200,  // плaвнo меняем прoзрaчнoсть нa 0 и oднoвременнo двигaем oкнo вверх
                        function () { // пoсле aнимaции
                            $(this).css('display', 'none'); // делaем ему display: none;
                            $('#overlay').fadeOut(400); // скрывaем пoдлoжку
                        }
                    );
            });
        });
    </script>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        table {
            padding: 0;
            line-height: 2;
            top: 0;
            table-layout: fixed;
            overflow: hidden;
        }

        a {
            text-decoration: none; /* Отменяем подчеркивание у ссылки */
        }

        form {
            margin: 0; /* Убираем отступы */
        }

        div {
            font-size: 125%
        }

        table.news {
            border-radius: 10px;
        }

        .modalDialog {
            position: fixed;
            font-family: Arial, Helvetica, sans-serif;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.8);
            z-index: 99999;
            -webkit-transition: opacity 400ms ease-in;
            -moz-transition: opacity 400ms ease-in;
            transition: opacity 400ms ease-in;
            display: none;
            pointer-events: none;
        }

        .modalDialog:target {
            display: block;
            pointer-events: auto;
        }

        .modalDialog > div {
            width: 400px;
            position: relative;
            margin: 10% auto;
            padding: 5px 20px 13px 20px;
            border-radius: 10px;
            background: #fff;
            background: -moz-linear-gradient(#fff, #999);
            background: -webkit-linear-gradient(#fff, #999);
            background: -o-linear-gradient(#fff, #999);

        }

        .close {
            background: #606061;
            color: #FFFFFF;
            line-height: 25px;
            position: absolute;
            right: -12px;
            text-align: center;
            top: -10px;
            width: 24px;
            text-decoration: none;
            font-weight: bold;
            -webkit-border-radius: 12px;
            -moz-border-radius: 12px;
            border-radius: 12px;
            -moz-box-shadow: 1px 1px 3px #000;
            -webkit-box-shadow: 1px 1px 3px #000;
            box-shadow: 1px 1px 3px #000;
        }

        .close:hover {
            background: gray;
        }
    </style>
</head>
<body link="black" alink="black" vlink="black" bgcolor="#F5F5F5">

<table width="1100px" height="50px" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="white">
    <tr>
        <td width="18%" bgcolor="#F5F5F5" align="center">
            <a href="#addCategory"><button type="button">Создать категорию</button></a>
        </td>
        <td width="60%" align="right">
            <a href="${pageContext.request.contextPath}/search">
            <button type="button">Поиск</button></a>
        </td>
        <td width="2%"></td>
        <td width="20%">
            <a href="${pageContext.request.contextPath}/create">
                <button type="button">Добавить</button>
            </a>
        </td>

    </tr>
</table>

<table width="1100px" height=100% border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td width="18%" align="center" valign="top" bgcolor="#F5F5F5" style="line-height: 1.3;">
            <c:forEach var="category" items="${categoryList}">
                <a href="/?category=${category.categoryName}">${category.categoryName}</a><br>
            </c:forEach>

        </td>
        <td align="left" valign="top" bgcolor="white">
            <c:forEach var="news" items="${newsList}">
                <table class="news" width=70% height="250px" border="0" align="center" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#F5F5F5" height="10%" style="line-height: 1;">
                        <td width="100%" colspan="5">
                            <a href="${pageContext.request.contextPath}/read?id=${news.newsId}">&nbsp;${news.newsName}</a>
                        </td>
                    </tr>
                    <tr height="80%">
                        <td width="100%" colspan="5">
                            <c:choose>
                                <c:when test="${news.getContent().length() <= 500}">
                                    ${news.getContent()}
                                </c:when>
                                <c:otherwise>
                                    ${news.getContent().subSequence(0,500)}...
                                </c:otherwise>
                            </c:choose>
                        </td>
                    </tr>
                    <tr bgcolor="#F5F5F5" style="line-height: 1;">
                        <td colspan="4">
                            <c:forEach var="cat" items="${news.categorySet}">
                                <a href="/?category=${cat.categoryName}">&nbsp;${cat.categoryName}</a>
                            </c:forEach>
                        </td>
                        <td align="center">
                                ${news.publishedDate.toString().subSequence(0,10)}
                        </td>
                    </tr>
                </table>
                <br>
            </c:forEach>

        </td>
    </tr>
</table>
<div id="addCategory" class="modalDialog">
    <div>
        <a href="#close" title="Закрыть" class="close">X</a>
        <form method="post"
              action="${pageContext.request.contextPath}/add">
            <p align="center"><input type="text" name="data" size="27%">
                <input type="submit" value=" Создать "></p>
        </form>
    </div>
</div>

</body>
</html>