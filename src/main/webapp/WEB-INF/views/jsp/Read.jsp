<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="style.css"/>
    <title>${news.newsName}</title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        table {
            padding: 0;
            line-height: 2;
            top: 0;
            table-layout: fixed;
            overflow: hidden;
        }

        a {
            text-decoration: none; /* Отменяем подчеркивание у ссылки */
        }

        form {
            margin: 0; /* Убираем отступы */
        }

        div {
            font-size: 125%
        }

        table.news {
            border-radius: 10px;
        }

    </style>
</head>
<body link="black" alink="black" vlink="black" bgcolor="#F5F5F5">

<table width="1100px" height="50px" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="white">
    <tr>
        <td width="18%" bgcolor="#F5F5F5"></td>
        <td width="58%"></td>
        <td width="10%">
            <a href="${pageContext.request.contextPath}/update?id=${news.newsId}">
                <button type="button">Редактировать</button>
            </a>
        </td>
        <td width="10%">
            <a href="${pageContext.request.contextPath}/delete?id=${news.newsId}">
                <button type="button">Удалить</button>
            </a>
        </td>
        <td width="4%"></td>

    </tr>
</table>

<table width="1100px" height=100% border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td width="18%" align="center" valign="top" bgcolor="#F5F5F5" style="line-height: 1.3;">
            <c:forEach var="category" items="${categoryList}">
                <a href="/?category=${category.categoryName}">${category.categoryName}</a><br>
            </c:forEach>

        </td>
        <td align="left" valign="top" bgcolor="white">
                <table class="news" width=80% height="80%" border="0" align="center" cellspacing="0" cellpadding="0">
                    <tr bgcolor="#F5F5F5" height="10%" style="line-height: 1;">
                        <td width="100%" colspan="5">
                            <a href="${pageContext.request.contextPath}/read?id=${news.newsId}">&nbsp;${news.newsName}</a>
                        </td>
                    </tr>
                    <tr height="80%">
                        <td width="100%" colspan="5"> ${news.getContent()}</td>
                    </tr>
                    <tr bgcolor="#F5F5F5" height="10%" style="line-height: 1;">
                        <td colspan="4">
                            <c:forEach var="cat" items="${news.categorySet}">
                                <a href="/?category=${cat.categoryName}">&nbsp;${cat.categoryName}</a>
                            </c:forEach>
                        </td>
                        <td align="center">
                                ${news.publishedDate.toString().subSequence(0,10)}
                        </td>
                    </tr>
                </table>
                <br>

        </td>
    </tr>
</table>

</body>
</html>