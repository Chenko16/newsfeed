<%--
  Created by IntelliJ IDEA.
  User: Ченко
  Date: 06.09.2018
  Time: 13:07
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8"  pageEncoding="UTF-8" language="java" %>
<html>
<head>
    <title>Create article</title>
</head>
<body bgcolor="#F5F5F5">

<table width="900px" height="30px" border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td width="80%"> </td>
        <td width="20%">
        </td>

    </tr>
</table>

<table width="900px" height=100% border="0" align="center" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <form action="/save" method="post">
                <p><input type="text" name="name" value="${news.newsName}" size="40"></p>
                <p><textarea cols="100" name="content" rows="25">${news.content}</textarea></p>
                <p><select name="category">
                    <c:forEach var="category" items="${categoryList}">
                        <option value="${category.categoryName}">${category.categoryName}</option>
                    </c:forEach>
                    </select>
                    <input type="hidden" name="id" value="${news.newsId}">
                    <input type="submit" value="Сохранить">
                </p>
            </form>

        </td>
    </tr>
</table>




</body>
</html>
