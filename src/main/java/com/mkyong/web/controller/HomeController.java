package com.mkyong.web.controller;

import com.mkyong.dao.CategoryEntity;
import com.mkyong.dao.HibernateSessionFactory;
import com.mkyong.dao.NewsEntity;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.UnsupportedEncodingException;
import java.util.*;

@Controller
public class HomeController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String showNews(ModelMap model, @RequestParam(value = "category", defaultValue = "") String category) {
        CategoryEntity cur = null;
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        if (category.equals("")) {
            cur = categoryList.iterator().next();
        } else {
            for(CategoryEntity cat: categoryList){
                if(cat.getCategoryName().equals(category))
                    cur = cat;
            }
        }
        model.addAttribute("newsList", cur.getNewsSet().toArray());
        model.addAttribute("categoryList", categoryList);

        return "index";
    }


    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(ModelMap model) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        categoryList.remove(0);
        NewsEntity news = new NewsEntity();
        news.setNewsName("Название");
        model.addAttribute("news", news);
        model.addAttribute("categoryList", categoryList);

        return "Create";
    }


    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveArticle(ModelMap model, @RequestParam("name") String name, @RequestParam("id") String id,
                              @RequestParam("content") String content, @RequestParam("category") String category) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        try {
            name = new String(name.getBytes("ISO-8859-1"),"UTF-8");
            content = new String(content.getBytes("ISO-8859-1"),"UTF-8");
            category = new String(category.getBytes("ISO-8859-1"),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        Set<NewsEntity> newsSet = categoryList.iterator().next().getNewsSet();
        Set<CategoryEntity> categorySet = new HashSet<CategoryEntity>();// категории данной новости

        for(CategoryEntity cat:categoryList){
            if(cat.getCategoryName().equals("Всё") || cat.getCategoryName().equals(category)){
                categorySet.add(cat);
            }
        }

        int newsId = Integer.parseInt(id);
        if(newsId == 0){
            NewsEntity news = new NewsEntity();
            news.setNewsName(name);
            news.setContent(content);
            news.setPublishedDate(new Date());
            news.setCategorySet(categorySet);
            session.save(news);
        }else{
            sql = "From " + NewsEntity.class.getSimpleName();
            List<NewsEntity> newsList = session.createQuery(sql).list();
            NewsEntity cur = null;

            for(NewsEntity news: newsList){
                if(news.getNewsId() == Integer.parseInt(id)){
                    cur = news;
                    break;
                }
            }
            cur.setNewsName(name);
            cur.setContent(content);
            cur.setCategorySet(categorySet);
        }

        session.flush();
        session.getTransaction().commit();

        model.addAttribute("categoryList", categoryList);
        model.addAttribute("newsList", newsSet.toArray());

        return "index";
    }


    @RequestMapping(value = "/read", method = RequestMethod.GET)
    public String read(ModelMap model, @RequestParam("id") String id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        String sql = "From " + NewsEntity.class.getSimpleName();
        List<NewsEntity> newsList = session.createQuery(sql).list();
        NewsEntity cur = null;

        for(NewsEntity news: newsList){
            if(news.getNewsId() == Integer.parseInt(id)){
                cur = news;
                break;
            }
        }

        sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();

        model.addAttribute("categoryList", categoryList);
        model.addAttribute("news", cur);

        return "Read";
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(ModelMap model, @RequestParam("id") String id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        String sql = "From " + NewsEntity.class.getSimpleName();
        List<NewsEntity> newsList = session.createQuery(sql).list();
        NewsEntity cur = null;

        for(NewsEntity news: newsList){
            if(news.getNewsId() == Integer.parseInt(id)){
                cur = news;
                break;
            }
        }

        sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        categoryList.remove(0);

        model.addAttribute("categoryList", categoryList);
        model.addAttribute("news", cur);

        return "Create";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String saveArticle(ModelMap model, @RequestParam("id") String id) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        String sql = "From " + NewsEntity.class.getSimpleName();
        List<NewsEntity> newsList = session.createQuery(sql).list();
        NewsEntity cur = null;

        for(NewsEntity news: newsList){
            if(news.getNewsId() == Integer.parseInt(id)){
                cur = news;
                break;
            }
        }
        session.delete(cur);
        session.flush();
        session.getTransaction().commit();

        sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        Set<NewsEntity> newsSet = categoryList.iterator().next().getNewsSet();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("newsList", newsSet.toArray());

        return "index";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(ModelMap model, @RequestParam("data") String name) {
        try {
            name = new String(name.getBytes("ISO-8859-1"),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();

        CategoryEntity news = new CategoryEntity();
        news.setCategoryName(name);
        session.save(news);

        session.flush();
        session.getTransaction().commit();

        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        model.addAttribute("categoryList", categoryList);
        model.addAttribute("newsList", categoryList.get(categoryList.size()-1).getNewsSet().toArray());

        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(ModelMap model) {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        model.addAttribute("categoryList", categoryList);

        return "search";
    }


    @RequestMapping(value = "/find", method = RequestMethod.POST)
    public String find(ModelMap model, @RequestParam("name") String name,
                       @RequestParam("content") String content, @RequestParam("category") String category) {
        try {
            name = new String(name.getBytes("ISO-8859-1"),"UTF-8");
            content = new String(content.getBytes("ISO-8859-1"),"UTF-8");
            category = new String(category.getBytes("ISO-8859-1"),"UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        session.beginTransaction();
        String sql = "From " + CategoryEntity.class.getSimpleName();
        List<CategoryEntity> categoryList = session.createQuery(sql).list();
        CategoryEntity curCategory = null;
        for(CategoryEntity cat: categoryList){
            if(cat.getCategoryName().equals(category)){
                curCategory = cat;
                break;
            }
        }
        List<NewsEntity> foundNews = new ArrayList<NewsEntity>();
        if(!content.equals("")){
            for(NewsEntity news: curCategory.getNewsSet()){
                if(news.getNewsName().contains(name) || news.getContent().contains(content))
                    foundNews.add(news);
            }
        }else{
            for(NewsEntity news: curCategory.getNewsSet()){
                if(news.getNewsName().contains(name))
                    foundNews.add(news);
            }
        }

        model.addAttribute("categoryList", categoryList);
        model.addAttribute("newsList", foundNews);
        return "index";
    }

}