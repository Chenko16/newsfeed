package com.mkyong.dao;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "News", schema = "dbo", catalog = "AWS")
public class NewsEntity implements Comparable<NewsEntity>{
    private long newsId;
    private String newsName;
    private String content;
    private Date publishedDate;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="newsId", nullable = false, unique = true)
    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    @Basic
    @Column(name = "newsName", nullable = false, length = 50)
    public String getNewsName() {
        return newsName;
    }

    public void setNewsName(String newsName) {
        this.newsName = newsName;
    }

    @Basic
    @Column(name = "content", nullable = false, length = 8000)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "publishedDate", nullable = false)
    public Date getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(Date publishedDate) {
        this.publishedDate = publishedDate;
    }

    @ManyToMany
    @JoinTable(name = "NewsOfCategory",
            joinColumns = {@JoinColumn(name = "newsId")},
            inverseJoinColumns = {@JoinColumn(name = "categoryId")})
    private Set<CategoryEntity> categorySet = new TreeSet<CategoryEntity>();

    public Set<CategoryEntity> getCategorySet() {return categorySet;}

    public void setCategorySet(Set<CategoryEntity> news) {categorySet = news;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NewsEntity that = (NewsEntity) o;

        if (newsId != that.newsId) return false;
        if (newsName != null ? !newsName.equals(that.newsName) : that.newsName != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (publishedDate != null ? !publishedDate.equals(that.publishedDate) : that.publishedDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + (newsName != null ? newsName.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (publishedDate != null ? publishedDate.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(NewsEntity o) {
        if(this.getPublishedDate().after(o.getPublishedDate()))
            return -1;
        else
            if(this.getPublishedDate().before(o.getPublishedDate()))
                return  1;
            else
                return 0;
    }
}
